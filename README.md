# uBot-7 mobile manipulator #

This repository holds the CAD files (Solidworks) for the uBot-7 mobile manipulator. The robot is the latest model of the uBot series developed by the [Laboratory for Perceptual Robotics](http://lpr.cs.umass.edu) at the [University of Massachusetts Amherst](http://www.umass.edu). More information on the robot can be found at [http://lpr.cs.umass.edu/ubot](http://lpr.cs.umass.edu/ubot) or in this publication: [uBot-7: A Dynamically Balancing Mobile Manipulator with Series Elastic Actuators](http://lpr.cs.umass.edu/uploads/Robots/ruiken_humanoids_2017.pdf). 

Please note that the design files for the robot head are hosted in a separate repository:
[https://bitbucket.org/umass_lpr/ubot_3dof_head](https://bitbucket.org/umass_lpr/ubot_3dof_head).

![123](https://bitbucket.org/umass_lpr/ubot-7/raw/f6e85ea8ad06e9982d17a05ae2f6c8a49db7936c/u7_300h.png)


### Contact ###

If you have questions, please contact [Roderic Grupen](http://www-robotics.cs.umass.edu/~grupen) or [Dirk Ruiken](http://www-robotics.cs.umass.edu/~ruiken).


### Citing uBot-7 ###

If you would like to use the uBot-7 models or reference the robot in academic papers, please cite 
[uBot-7: A Dynamically Balancing Mobile Manipulator with Series Elastic Actuators](http://lpr.cs.umass.edu/uploads/Robots/ruiken_humanoids_2017.pdf).

![234](https://bitbucket.org/umass_lpr/ubot-7/raw/f6e85ea8ad06e9982d17a05ae2f6c8a49db7936c/u7_balance_300w.jpg)
![234](https://bitbucket.org/umass_lpr/ubot-7/raw/f6e85ea8ad06e9982d17a05ae2f6c8a49db7936c/u7_prone_scoot_300h.jpg)